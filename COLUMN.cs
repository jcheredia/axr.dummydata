﻿
namespace AXR.DummyData
{
    class Column
    {
        public string column_name { get; set; }
        public string data_type { get; set; }
        public bool is_primary_key { get; set; } = false;
    }
}
