﻿using MoreLinq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace AXR.DummyData
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Regex _regex = new Regex("[^0-9]");
        private const string CUSTSETTLEMENT_TABLE = "CUSTSETTLEMENT";
        private const string CUSTTABLE_TABLE = "CUSTTABLE";
        private const string CUSTTRANS_TABLE = "CUSTTRANS";
        //private const string CUST_TABLE = "CUST";

        private readonly ArrayList INTEGERS = new ArrayList(new string[] { "bigint", "int" });
        private readonly ArrayList STRINGS = new ArrayList(new string[] { "ntext", "nvarchar", "varchar" });
        private readonly ArrayList DATES = new ArrayList(new string[] { "datetime", "date" });
        private readonly ArrayList DECIMALS = new ArrayList(new string[] { "numeric" });

        private const string CUSTSETTLEMENT_INSERT_SQL = "INSERT INTO [dbo].[CUSTSETTLEMENT] (" +
            "[TRANSRECID]" +
            ",[TRANSDATE]" +
            ",[OFFSETTRANSVOUCHER]" +
            ",[ACCOUNTNUM]" +
            ",[SETTLEAMOUNTCUR]" +
            ",[SETTLEAMOUNTMST]" +
            ",[EXCHADJUSTMENT]" +
            ",[SETTLEMENTVOUCHER]" +
            ",[TRANSCOMPANY]" +
            ",[OFFSETRECID]" +
            ",[DUEDATE]" +
            ",[UTILIZEDCASHDISC]" +
            ",[CUSTCASHDISCDATE]" +
            ",[LASTINTERESTDATE]" +
            ",[PENNYDIFF]" +
            ",[CANBEREVERSED]" +
            ",[CASHDISCACCOUNT]" +
            ",[DIMENSION]" +
            ",[DIMENSION2_]" +
            ",[DIMENSION3_]" +
            ",[SETTLETAX1099AMOUNT]" +
            ",[SETTLETAX1099STATEAMOUNT]" +
            ",[OFFSETCOMPANY]" +
            ",[OFFSETACCOUNTNUM]" +
            ",[SETTLEMENTGROUP]" +
            ",[CREATEDDATETIME]" +
            ",[DEL_CREATEDTIME]" +
            ",[CREATEDBY]" +
            ",[RECVERSION]" +
            ",[EUSALESLIST]" +
            ",[DATAAREAID]" +
            ",[RECID])" +
            " VALUES ";

        //private const string CUST_INSERT_SQL = CUSTSETTLEMENT_INSERT_SQL;

        private const string CUSTTABLE_INSERT_SQL = "INSERT INTO [dbo].[CUSTTABLE] (" +
            ",[NAME]" +
            ",[ADDRESS]" +
            ",[PHONE]" +
            ",[TELEFAX]" +
            ",[INVOICEACCOUNT]" +
            ",[CUSTGROUP]" +
            ",[LINEDISC]" +
            ",[PAYMTERMID]" +
            ",[CASHDISC]" +
            ",[CURRENCY]" +
            ",[INTERCOMPANYAUTOCREATEORDERS]" +
            ",[SALESGROUP]" +
            ",[BLOCKED]" +
            ",[ONETIMECUSTOMER]" +
            ",[ACCOUNTSTATEMENT]" +
            ",[CREDITMAX]" +
            ",[MANDATORYCREDITLIMIT]" +
            ",[DIMENSION]" +
            ",[DIMENSION2_]" +
            ",[DIMENSION3_]" +
            ",[VENDACCOUNT]" +
            ",[TELEX]" +
            ",[PRICEGROUP]" +
            ",[MULTILINEDISC]" +
            ",[ENDDISC]" +
            ",[VATNUM]" +
            ",[COUNTRYREGIONID]" +
            ",[INVENTLOCATION]" +
            ",[DLVTERM]" +
            ",[DLVMODE]" +
            ",[MARKUPGROUP]" +
            ",[CLEARINGPERIOD]" +
            ",[ZIPCODE]" +
            ",[STATE]" +
            ",[COUNTY]" +
            ",[URL]" +
            ",[EMAIL]" +
            ",[CELLULARPHONE]" +
            ",[PHONELOCAL]" +
            ",[FREIGHTZONE]" +
            ",[CREDITRATING]" +
            ",[TAXGROUP]" +
            ",[STATISTICSGROUP]" +
            ",[PAYMMODE]" +
            ",[COMMISSIONGROUP]" +
            ",[BANKACCOUNT]" +
            ",[PAYMSCHED]" +
            ",[NAMEALIAS]" +
            ",[CONTACTPERSONID]" +
            ",[INVOICEADDRESS]" +
            ",[OURACCOUNTNUM]" +
            ",[SALESPOOLID]" +
            ",[INCLTAX]" +
            ",[CUSTITEMGROUPID]" +
            ",[NUMBERSEQUENCEGROUP]" +
            ",[LANGUAGEID]" +
            ",[PAYMDAYID]" +
            ",[LINEOFBUSINESSID]" +
            ",[DESTINATIONCODEID]" +
            ",[GIROTYPE]" +
            ",[SUPPITEMGROUPID]" +
            ",[GIROTYPEINTERESTNOTE]" +
            ",[TAXLICENSENUM]" +
            ",[WEBSALESORDERDISPLAY]" +
            ",[PAYMSPEC]" +
            ",[BANKCENTRALBANKPURPOSETEXT]" +
            ",[BANKCENTRALBANKPURPOSECODE]" +
            ",[CITY]" +
            ",[STREET]" +
            ",[PAGER]" +
            ",[SMS]" +
            ",[INTERCOMPANYALLOWINDIRECTCRE80]" +
            ",[PACKMATERIALFEELICENSENUM]" +
            ",[TAXBORDERNUMBER_FI]" +
            ",[EINVOICEEANNUM]" +
            ",[FISCALCODE]" +
            ",[DLVREASON]" +
            ",[GIROTYPECOLLECTIONLETTER]" +
            ",[SALESCALENDARID]" +
            ",[CUSTCLASSIFICATIONID]" +
            ",[INTERCOMPANYDIRECTDELIVERY]" +
            ",[ENTERPRISENUMBER]" +
            ",[SHIPCARRIERACCOUNT]" +
            ",[GIROTYPEPROJINVOICE]" +
            ",[INVENTSITEID]" +
            ",[ORDERENTRYDEADLINEGROUPID]" +
            ",[SHIPCARRIERID]" +
            ",[SHIPCARRIERFUELSURCHARGE]" +
            ",[SHIPCARRIERBLINDSHIPMENT]" +
            ",[PARTYTYPE]" +
            ",[PARTYID]" +
            ",[SHIPCARRIERACCOUNTCODE]" +
            ",[PROJPRICEGROUP]" +
            ",[GIROTYPEFREETEXTINVOICE]" +
            ",[SYNCENTITYID]" +
            ",[SYNCVERSION]" +
            ",[MEMO]" +
            ",[SALESDISTRICTID]" +
            ",[SEGMENTID]" +
            ",[SUBSEGMENTID]" +
            ",[RFIDITEMTAGGING]" +
            ",[RFIDCASETAGGING]" +
            ",[RFIDPALLETTAGGING]" +
            ",[COMPANYCHAINID]" +
            ",[MAINCONTACTID]" +
            ",[COMPANYIDSIRET]" +
            ",[COMPANYIDNAF]" +
            ",[IDENTIFICATIONNUMBER]" +
            ",[PARTYCOUNTRY]" +
            ",[PARTYSTATE]" +
            ",[ORGID]" +
            ",[PAYMIDTYPE]" +
            ",[FACTORINGACCOUNT]" +
            ",[PBACUSTGROUPID]" +
            ",[MODIFIEDDATETIME]" +
            ",[CREATEDDATETIME]" +
            ",[RECVERSION]" +
            ",[RECID]" +
            ",[DEL_PRINTMODULETYPE]" +
            ",[DEL_REFZIPCODE]" +
            ",[DEL_NPIBANKINGPAYMIDTYPE]" +
            ",[DEL_NPIBANKINGFACTORINGACCOUNT]" +
            ",[GIROTYPEACCOUNTSTATEMENT]" +
            ",[COMPANYTYPE_MX]" +
            ",[RFC_MX]" +
            ",[CURP_MX]" +
            ",[STATEINSCRIPTION_MX]" +
            ",[RESIDENCEFOREIGNCOUNTRYREGI276]" +
            ",[BIRTHCOUNTYCODE_IT]" +
            ",[BIRTHDATE_IT]" +
            ",[BIRTHPLACE_IT]" +
            ",[EINVOICE]" +
            ",[CREDITCARDADDRESSVERIFICATION]" +
            ",[CREDITCARDCVC]" +
            ",[CREDITCARDADDRESSVERIFICATI292]" +
            ",[CREDITCARDADDRESSVERIFICATI293]" +
            ",[USECASHDISC]" +
            ",[CASHDISCBASEDAYS]" +
            ",[ACCOUNTNUM]" +
            ",[DATAAREAID])" +
            " VALUES ";

        private const string CUSTTRANS_INSERT_SQL = "INSERT INTO [dbo].[CUSTTRANS] (" +
            "[ACCOUNTNUM]" +
            ",[TRANSDATE]" +
            ",[VOUCHER]" +
            ",[INVOICE]" +
            ",[TXT]" +
            ",[AMOUNTCUR]" +
            ",[SETTLEAMOUNTCUR]" +
            ",[AMOUNTMST]" +
            ",[SETTLEAMOUNTMST]" +
            ",[CURRENCYCODE]" +
            ",[DUEDATE]" +
            ",[LASTSETTLEVOUCHER]" +
            ",[LASTSETTLEDATE]" +
            ",[LASTEXCHADJVOUCHER]" +
            ",[CLOSED]" +
            ",[TRANSTYPE]" +
            ",[APPROVEDBY]" +
            ",[APPROVED]" +
            ",[DIMENSION]" +
            ",[DIMENSION2_]" +
            ",[DIMENSION3_]" +
            ",[EXCHADJUSTMENT]" +
            ",[DOCUMENTNUM]" +
            ",[DOCUMENTDATE]" +
            ",[LASTEXCHADJRATE]" +
            ",[FIXEDEXCHRATE]" +
            ",[LASTEXCHADJ]" +
            ",[CORRECT]" +
            ",[BANKCENTRALBANKPURPOSECODE]" +
            ",[BANKCENTRALBANKPURPOSETEXT]" +
            ",[SETTLEMENT]" +
            ",[INTEREST]" +
            ",[COLLECTIONLETTER]" +
            ",[POSTINGPROFILECLOSE]" +
            ",[EXCHRATESECOND]" +
            ",[EXCHRATE]" +
            ",[LASTSETTLEACCOUNTNUM]" +
            ",[COMPANYBANKACCOUNTID]" +
            ",[THIRDPARTYBANKACCOUNTID]" +
            ",[PAYMMODE]" +
            ",[PAYMREFERENCE]" +
            ",[PAYMMETHOD]" +
            ",[CASHPAYMENT]" +
            ",[CONTROLNUM]" +
            ",[DELIVERYMODE]" +
            ",[POSTINGPROFILE]" +
            ",[OFFSETRECID]" +
            ",[EUROTRIANGULATION]" +
            ",[ORDERACCOUNT]" +
            ",[CASHDISCCODE]" +
            ",[PREPAYMENT]" +
            ",[PAYMSPEC]" +
            ",[CUSTEXCHADJUSTMENTREALIZED]" +
            ",[CUSTEXCHADJUSTMENTUNREALIZED]" +
            ",[PAYMMANLACKDATE]" +
            ",[PAYMMANBATCH]" +
            ",[PAYMID]" +
            ",[BILLOFEXCHANGEID]" +
            ",[BILLOFEXCHANGESTATUS]" +
            ",[BILLOFEXCHANGESEQNUM]" +
            ",[BANKREMITTANCEFILEID]" +
            ",[COLLECTIONLETTERCODE]" +
            ",[INVOICEPROJECT]" +
            ",[LASTSETTLECOMPANY]" +
            ",[CANCELLEDPAYMENT]" +
            ",[REASONREFRECID]" +
            ",[TAXINVOICESALESID]" +
            ",[MODIFIEDDATETIME]" +
            ",[DEL_MODIFIEDTIME]" +
            ",[MODIFIEDBY]" +
            ",[MODIFIEDTRANSACTIONID]" +
            ",[CREATEDDATETIME]" +
            ",[DEL_CREATEDTIME]" +
            ",[CREATEDBY]" +
            ",[CREATEDTRANSACTIONID]" +
            ",[DATAAREAID]" +
            ",[RECVERSION]" +
            ",[RECID]" +
            ",[DEL_OCRLINE]" +
            ",[CER_BLOQUEOCONGELADO]" +
            ",[CER_BLOQUEOCASTIGADO])" +
            "VALUES (" +
            "@ACCOUNTNUM" +
            ",@TRANSDATE" +
            ",@VOUCHER" +
            ",@INVOICE" +
            ",@TXT" +
            ",@AMOUNTCUR" +
            ",@SETTLEAMOUNTCUR" +
            ",@AMOUNTMST" +
            ",@SETTLEAMOUNTMST" +
            ",@CURRENCYCODE" +
            ",@DUEDATE" +
            ",@LASTSETTLEVOUCHER" +
            ",@LASTSETTLEDATE" +
            ",@LASTEXCHADJVOUCHER" +
            ",@CLOSED" +
            ",@TRANSTYPE" +
            ",@APPROVEDBY" +
            ",@APPROVED" +
            ",@DIMENSION" +
            ",@DIMENSION2_" +
            ",@DIMENSION3_" +
            ",@EXCHADJUSTMENT" +
            ",@DOCUMENTNUM" +
            ",@DOCUMENTDATE" +
            ",@LASTEXCHADJRATE" +
            ",@FIXEDEXCHRATE" +
            ",@LASTEXCHADJ" +
            ",@CORRECT" +
            ",@BANKCENTRALBANKPURPOSECODE" +
            ",@BANKCENTRALBANKPURPOSETEXT" +
            ",@SETTLEMENT" +
            ",@INTEREST" +
            ",@COLLECTIONLETTER" +
            ",@POSTINGPROFILECLOSE" +
            ",@EXCHRATESECOND" +
            ",@EXCHRATE" +
            ",@LASTSETTLEACCOUNTNUM" +
            ",@COMPANYBANKACCOUNTID" +
            ",@THIRDPARTYBANKACCOUNTID" +
            ",@PAYMMODE" +
            ",@PAYMREFERENCE" +
            ",@PAYMMETHOD" +
            ",@CASHPAYMENT" +
            ",@CONTROLNUM" +
            ",@DELIVERYMODE" +
            ",@POSTINGPROFILE" +
            ",@OFFSETRECID" +
            ",@EUROTRIANGULATION" +
            ",@ORDERACCOUNT" +
            ",@CASHDISCCODE" +
            ",@PREPAYMENT" +
            ",@PAYMSPEC" +
            ",@CUSTEXCHADJUSTMENTREALIZED" +
            ",@CUSTEXCHADJUSTMENTUNREALIZED" +
            ",@PAYMMANLACKDATE" +
            ",@PAYMMANBATCH" +
            ",@PAYMID" +
            ",@BILLOFEXCHANGEID" +
            ",@BILLOFEXCHANGESTATUS" +
            ",@BILLOFEXCHANGESEQNUM" +
            ",@BANKREMITTANCEFILEID" +
            ",@COLLECTIONLETTERCODE" +
            ",@INVOICEPROJECT" +
            ",@LASTSETTLECOMPANY" +
            ",@CANCELLEDPAYMENT" +
            ",@REASONREFRECID" +
            ",@TAXINVOICESALESID" +
            ",@MODIFIEDDATETIME" +
            ",@DEL_MODIFIEDTIME" +
            ",@MODIFIEDBY" +
            ",@MODIFIEDTRANSACTIONID" +
            ",@CREATEDDATETIME" +
            ",@DEL_CREATEDTIME" +
            ",@CREATEDBY" +
            ",@CREATEDTRANSACTIONID" +
            ",@DATAAREAID" +
            ",@RECVERSION" +
            ",@RECID" +
            ",@DEL_OCRLINE" +
            ",@CER_BLOQUEOCONGELADO" +
            ",@CER_BLOQUEOCASTIGADO)";

        List<Table> tables;

        public MainWindow()
        {
            InitializeComponent();

            tables = new List<Table>();

            tables.Add(new Table(CUSTSETTLEMENT_TABLE, CUSTSETTLEMENT_INSERT_SQL, new List<string>(){ "DATAAREAID", "RECID" }));
            //tables.Add(new Table(CUST_TABLE, CUST_INSERT_SQL, new List<string>() { "DATAAREAID", "RECID" }));
            tables.Add(new Table(CUSTTABLE_TABLE, CUSTTABLE_INSERT_SQL, new List<string>(){ "ACCOUNTNUM", "DATAAREAID" }));
            tables.Add(new Table(CUSTTRANS_TABLE, CUSTTRANS_INSERT_SQL, new List<string>(){ "RECID", "DATAAREAID" }));

            foreach(Table table in tables)
            {
                this._SetFields(table);
            }
        }

        /*
         * Función que maneja el evento "PreviewTextInput".
         * Usada para sólo permitir números en los TextBox de la aplicación
         * 
        */
        private void ONLY_NUMBER_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = this.IsTextAllowed(e.Text);
        }

        /*
         * Función para sólo admitir números
         * 
         * text: Texto a revisar
        */
        private bool IsTextAllowed(string text)
        {
            return this._regex.IsMatch(text);
        }

        /*
         * Función que setea las columnas no únicas de una tabla
         * 
         * table_name: Nombre de la tabla de la que obtendremos sus columnas
         * fields: Variable donde se guardan las columnas de la tabla
         * ids: Nombres de las columnas cuyos valores deben ser únicos
        */
        private void _SetFields(Table table)
        {
            if (!string.IsNullOrEmpty(table.name))
            {
                // Creamos la query
                string sql = "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TABLE_NAME";

                using (var ctx = new AX2009Entities())
                {
                    try
                    {
                        // Obtenemos las columnas de la tabla
                        table.columns = ctx.Database.SqlQuery<Column>(sql, new SqlParameter("@TABLE_NAME", table.name)).ToList();

                        // Seteamos las primary key
                        table.columns = table.columns.Select(x => { x.is_primary_key = table.primary_keys.Contains(x.column_name); return x; }).ToList();
                    }
                    catch(Exception e) {
                        MessageBox.Show("Ha ocurrido un error al obtener los metadatos de la tabla " + table.name + "\n" + e.ToString());
                    }
                }
            }
        }

        private int _InsertRows(int num_rows, Table table)
        {
            if(num_rows > 0 && !string.IsNullOrEmpty(table.name))
            {
                using (var ctx = new AX2009Entities())
                {
                    bool primera_insercion = this._CheckFirstInsertion(table.name);
                    List<string> values = new List<string>();

                    //List<CUSTSETTLEMENT> elements = new List<CUSTSETTLEMENT>();
                    List<string> array_values = new List<string>();
                    var watch_code = Stopwatch.StartNew();

                    string value_string = "(";
                    values = new List<string>();
                    //CUSTSETTLEMENT element = new CUSTSETTLEMENT();

                    //List<PropertyInfo> properties = typeof(CUSTSETTLEMENT).GetProperties().ToList();
                    //List<SqlParameter> parameters = new List<SqlParameter>();

                    List<Column> not_primary_keys = table.columns.Where(x => !x.is_primary_key).ToList();
                    List<Column> primary_keys = table.columns.Except(not_primary_keys).ToList();

                    long[] indexes = Enumerable.Repeat<long>(-1, primary_keys.Count).ToArray();

                    string random_string = "'" + this._RandomString(2) + "'";
                    string random_int = this._RandomInt(5).ToString();
                    string random_decimal = this._RandomDecimal().ToString().Replace(",", ".");
                    string random_date = "'" + this._RandomDate().ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    string random_guid = "'" + new Guid("00000000-0000-0000-0000-000000000000").ToString() + "'";

                    foreach (Column column in not_primary_keys)
                    {
                        if (this.STRINGS.Contains(column.data_type))
                        {
                            // La columna es un tipo de string
                            //parameters.Add(new SqlParameter("@" + column.column_name, this._RandomString(2)));
                            //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, this._RandomString(2));
                            //values.Add("'" + this._RandomString(2) + "'");
                            values.Add(random_string);
                        }
                        else if (this.INTEGERS.Contains(column.data_type))
                        {
                            // La columna es un tipo de número
                            //parameters.Add(new SqlParameter("@" + column.column_name, this._RandomInt(5)));
                            //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, this._RandomInt(5));
                            //values.Add(this._RandomInt(5).ToString());
                            values.Add(random_int);
                        }
                        else if (this.DECIMALS.Contains(column.data_type))
                        {
                            // La columna es un tipo de decimal
                            //SqlParameter parameter = new SqlParameter("@" + column.column_name, SqlDbType.Decimal);

                            //parameter.Value = this._RandomDecimal();
                            //parameters.Add(parameter);
                            //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, this._RandomDecimal());
                            //values.Add(this._RandomDecimal().ToString().Replace(",", "."));
                            values.Add(random_decimal);
                        }
                        else if (this.DATES.Contains(column.data_type))
                        {
                            // La columna es un tipo de fecha
                            //SqlParameter parameter = new SqlParameter("@" + column.column_name, SqlDbType.Date);

                            //parameter.Value = this._RandomDate();
                            //parameters.Add(parameter);
                            //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, this._RandomDate());
                            //values.Add("'" + this._RandomDate().ToString("yyyy-MM-dd HH:mm:ss") + "'");
                            values.Add(random_date);
                        }
                        else
                        {
                            // La columna es un identificador único
                            //SqlParameter parameter = new SqlParameter("@" + column.column_name, SqlDbType.UniqueIdentifier);

                            //parameter.Value = new Guid("00000000-0000-0000-0000-000000000000");
                            //parameters.Add(parameter);
                            //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, new Guid("00000000-0000-0000-0000-000000000000"));
                            //values.Add("'" + new Guid("00000000-0000-0000-0000-000000000000").ToString() + "'");
                            values.Add(random_guid);
                        }
                    }

                    for (int i = 0; i < num_rows; i++)
                    {
                        value_string = "(";
                        List<string> pk_values = values.ToList();

                        try
                        {
                            int internal_index = 0;
                            foreach (Column column in primary_keys)
                            {
                                if (this.STRINGS.Contains(column.data_type))
                                {
                                    // La PK es un string. Generamos uno aleatorio
                                    //parameters.Add(new SqlParameter("@" + column.column_name, this._RandomString(3)));
                                    //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, this._RandomString(3));
                                    pk_values.Add("'" + this._RandomString(3) + "'");
                                }
                                else if (this.INTEGERS.Contains(column.data_type))
                                {
                                    // La PK es un long. Comprobamos si es la 1ª insercion
                                    long last = -1;

                                    if (primera_insercion) {
                                        last = 0;

                                        //parameters.Add(new SqlParameter("@" + column.column_name, last + 1));
                                        //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, last + 1);
                                        pk_values.Add((last + 1).ToString());

                                        indexes[internal_index] = last + 1;
                                        primera_insercion = false;
                                    } else { 
                                        //Obtenemos el último y aumentamos 1
                                        // Comprobamos si tenemos el último en local
                                        if (!indexes[internal_index].Equals(-1))
                                            last = indexes[internal_index];
                                        else
                                            last = this._GetLastLong(table.name, column.column_name);

                                        //parameters.Add(new SqlParameter("@" + column.column_name, last + 1));
                                        //properties.Find(x => x.Name.Equals(column.column_name)).SetValue(element, last + 1);
                                        pk_values.Add((last + 1).ToString());

                                        // Aumentamos en 1 la última PK
                                        indexes[internal_index] = last + 1;
                                    }
                                }

                                internal_index++;
                            }
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Se ha producido un error al añadir las PKs \n" + e.ToString());
                        }

                        value_string += string.Join(",", pk_values) + ")";
                        array_values.Add(value_string);
                        //elements.Add(element);
                    }
                    watch_code.Stop();
                    try
                    {
                        //ctx.Database.ExecuteSqlCommand(table.insert_sql, parameters.ToArray());
                        //ctx.CUSTSETTLEMENT.AddRange(elements);
                        //ctx.SaveChanges();
                        var watch_sql = Stopwatch.StartNew();
                        //ctx.Database.ExecuteSqlCommand(table.insert_sql + string.Join(",", array_values.GetRange(i*1000, array_values.Count)), new object[] { });
                        foreach (IEnumerable<string> batch in array_values.Batch(250))
                        {
                            ctx.Database.ExecuteSqlCommand(table.insert_sql + string.Join(",", batch), new object[] { });
                        }
                        watch_sql.Stop();

                        MessageBox.Show("WATCH_CODE: " + watch_code.Elapsed.TotalSeconds);
                        MessageBox.Show("WATCH_SQL: " + watch_sql.Elapsed.TotalSeconds);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Se ha producido un error al insertar una fila \n" + e.ToString());
                        return 0;
                    }

                    return 1;
                }
            }

            return 1;
        }

        private bool _CheckFirstInsertion(string table_name)
        {
            int num_rows = -1;

            try
            {
                using(var ctx = new AX2009Entities())
                {
                    switch (table_name)
                    {
                        case CUSTSETTLEMENT_TABLE:
                            num_rows = ctx.CUSTSETTLEMENT.Count();
                            break;
                        case CUSTTABLE_TABLE:
                            num_rows = ctx.CUSTTABLE.Count();
                            break;
                        case CUSTTRANS_TABLE:
                            num_rows = ctx.CUSTTRANS.Count();
                            break;
                        /*case CUST_TABLE:
                            num_rows = ctx.CUST.Count();
                            break;*/
                    }

                    return num_rows == -1 ? true : false;
                }
            } catch(Exception e)
            {
                MessageBox.Show("Ha ocurrido un error al comprobar la inserción. \n" + e.ToString());
                return false;
            }
        }

        private long _GetLastLong(string table_name, string column)
        {
            using(var ctx = new AX2009Entities())
            {
                string sql = "SELECT MAX(" + column + ") FROM " + table_name;

                return ctx.Database.SqlQuery<long>(sql).First();
            }
        }

        private string _GetLastString(string table_name, string column)
        {
            using (var ctx = new AX2009Entities())
            {
                string sql = "SELECT MAX(" + column + ") FROM " + table_name;

                return ctx.Database.SqlQuery<string>(sql).First();
            }
        }

        private Decimal _RandomDecimal()
        {
            Random random = new Random();

            return Convert.ToDecimal(random.NextDouble() * (1000 - 0));
        }

        private DateTime _RandomDate()
        {
            Random random = new Random();

            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;

            return start.AddDays(random.Next(range));
        }

        private int _RandomInt(int length)
        {
            Random random = new Random();

            return random.Next(0, 100);
        }

        private String _RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random random = new Random();

            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void CUSTTSETELEMENT_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            int num_rows = int.Parse(this.CUSTSETTELEMENT_QUANTITY.Text);

            int result = this._InsertRows(num_rows, tables.Where(x => x.name.Equals(CUSTSETTLEMENT_TABLE)).First());

            if (result == 1)
            {
                MessageBox.Show("Se han insertado las filas correctamente");
            }

            this.CUSTSETTELEMENT_QUANTITY.Text = "";
        }

        private void CUSTTABLE_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            int num_rows = int.Parse(this.CUSTTABLE_QUANTITY.Text);

            int result = this._InsertRows(num_rows, tables.Where(x => x.name.Equals(CUSTTABLE_TABLE)).First());

            if (result == 1)
            {
                MessageBox.Show("Se han insertado las filas correctamente");
            }

            this.CUSTTABLE_QUANTITY.Text = "";
        }

        private void CUSTTRANS_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            int num_rows = int.Parse(this.CUSTTRANS_QUANTITY.Text);

            int result = this._InsertRows(num_rows, tables.Where(x => x.name.Equals(CUSTTRANS_TABLE)).First());

            if (result == 1)
            {
                MessageBox.Show("Se han insertado las filas correctamente");
            }

            this.CUSTTRANS_QUANTITY.Text = "";
        }

        private void CUSTSETTLEMENT_DELETE_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            Table table = tables.Where(x => x.name.Equals(CUSTSETTLEMENT_TABLE)).First();

            if (table != null)
            {
                int result = this._DeleteTable(table);

                if(result == 1)
                {
                    MessageBox.Show("La tabla " + table.name + " se ha eliminado correctamente");
                }
            } else
                MessageBox.Show("Error. No existe la tabla seleccionada");
        }

        private void CUSTTABLE_DELETE_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            Table table = tables.Where(x => x.name.Equals(CUSTTABLE_TABLE)).First();

            if (table != null)
            {
                int result = this._DeleteTable(table);

                if (result == 1)
                {
                    MessageBox.Show("La tabla " + table.name + " se ha eliminado correctamente");
                }
            }
            else
                MessageBox.Show("Error. No existe la tabla seleccionada");
        }

        private void CUSTTRANS_DELETE_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            Table table = tables.Where(x => x.name.Equals(CUSTTRANS_TABLE)).First();

            if (table != null)
            {
                int result = this._DeleteTable(table);

                if (result == 1)
                {
                    MessageBox.Show("La tabla " + table.name + " se ha eliminado correctamente");
                }
            }
            else
                MessageBox.Show("Error. No existe la tabla seleccionada");
        }

        private int _DeleteTable(Table table)
        {
            if(table != null)
            {
                using (var ctx = new AX2009Entities())
                {
                    string sql = "DELETE FROM " + table.name;

                    try
                    {
                        ctx.Database.ExecuteSqlCommand(sql);
                    } catch(Exception e)
                    {
                        MessageBox.Show("Se ha producido un error al intentar borrar la tabla " + table.name + "\n" + e.ToString());
                        return 0;
                    }
                }
            }

            return 1;
        }

    }
}
