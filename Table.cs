﻿using System.Collections.Generic;

namespace AXR.DummyData
{
    class Table
    {
        public string name { get; set; }
        public string insert_sql { get; set; }
        public List<Column> columns { get; set; }
        public List<string> primary_keys { get; set; }

        public Table(string new_name, string new_insert_sql, List<string> new_primary_keys)
        {
            this.name = new_name;
            this.insert_sql = new_insert_sql;
            this.columns = new List<Column>();
            this.primary_keys = new_primary_keys;
        }

        public Table(string new_name, string new_insert_sql, List<Column> new_columns, List<string> new_primary_keys)
        {
            this.name = new_name;
            this.insert_sql = new_insert_sql;
            this.columns = new_columns;
            this.primary_keys = new_primary_keys;
        }
    }
}
